from django.contrib import admin
from backend.models import City, Univer, Faculty, Prepod

admin.site.register(City)
admin.site.register(Univer)
admin.site.register(Faculty)
admin.site.register(Prepod)
