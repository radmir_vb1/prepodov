from django.db import models

class City(models.Model):
	name = models.CharField(max_length=200)
	region = models.CharField(max_length=200)
	is_capital = models.BooleanField()
	population = models.IntegerField()

class Univer(models.Model):
	city = models.ForeignKey('City')
	name = models.CharField(max_length=100)

class Faculty(models.Model):
	univer = models.ForeignKey('Univer')
	name = models.CharField(max_length=200)

class Prepod(models.Model):
	univer = models.ForeignKey('Univer')
	faculty = models.ForeignKey('Faculty')
	first_name = models.CharField(max_length=100)
	middle_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)